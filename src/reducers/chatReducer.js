import {
  FETCH_DATA,
  SET_PRELOADER,
  SET_CHAT_DATA,
  ADD_MESSAGE,
  DELETE_MESSAGE,
  UPDATE_MESSAGE
} from "../actions/actionTypes";
import {v4 as uuidv4} from "uuid";

const initialState = {
  "chat": {
    "messages": [],
    "editModal": false,
    "preloader": true,
    "chatTitle": "FunChat",
    "currentUserId": uuidv4(),
    "currentUser": "Inna",
    "currentUserAvatar": ""
  }
}


function chatReducer(state = initialState, action = {}) {
  switch (action.type) {
    case SET_PRELOADER: {
      const {preloader} = action.payload;
      return {
        ...state,
        chat: {
          ...state.chat,
          preloader
        }
      };
    }
    case FETCH_DATA: {
      const {messages} = action.payload;
      return {
        ...state,
        chat: {
          ...state.chat,
          messages: [...state.chat.messages, ...messages]
        }
      };
    }
    case SET_CHAT_DATA: {
      const {messages} = action.payload;
      const messagesCount = messages.length;
      const usersCount = new Set(messages.map(it => it.userId)).size;
      const date = new Date(messages[messagesCount - 1]['createdAt']);
      const month = date.getMonth() + 1;
      const lastMessageDate = `${date.getDate()}.${month < 10 ? '0' + month : month}.${date.getFullYear()} ${date.getHours()}:${date.getMinutes()}`;
      return {
        ...state,
        chat: {
          ...state.chat,
          usersCount,
          messagesCount,
          lastMessageDate
        }
      };
    }
    case ADD_MESSAGE: {
      const {messageData} = action.payload;
      return {
        ...state,
        chat: {
          ...state.chat,
          messages: [...state.chat.messages, messageData]
        }
      };
    }
    case UPDATE_MESSAGE: {
      const {id, messageData} = action.payload;
      const updatedMessages = state.chat.messages.map(message => {
        if (message.id === id) {
          return {
            ...message,
            ...messageData
          };
        } else {
          return message;
        }
      });
      
      return {
        ...state,
        chat: {
          ...state.chat,
          messages: [...updatedMessages]
        }
      };
    }
    case DELETE_MESSAGE: {
      const {messageId} = action.payload;
      const filteredMessages = state.chat.messages.filter(message => message.id !== messageId);
      return {
        ...state,
        chat: {
          ...state.chat,
          messages: [...filteredMessages]
        }
      };
    }
    default:
      return state;
  }
  
}

export default chatReducer;