import React, {useState} from "react";
import './Input.css'
import {addMessage} from "../../actions/actions";
import {useDispatch, useSelector} from "react-redux";
import {v4 as uuidv4} from "uuid";

const Input = () => {
  const currentUserId = useSelector(
    (state) => state.chat.chat.currentUserId
  );
  const currentUser = useSelector(
    (state) => state.chat.chat.currentUser
  );
  const currentUserAvatar = useSelector(
    (state) => state.chat.chat.currentUserAvatar
  );
  
  const [messageText, setMessageText] = useState('');
  
  const dispatch = useDispatch();
  
  const handleOnChange = ({target}) => {
    setMessageText(target.value);
  }
  
  const handleMessageAdd = () => {
    const date = new Date(Date.now());
    const message = {
      id: uuidv4(),
      userId: currentUserId,
      avatar: currentUserAvatar,
      user: currentUser,
      text: messageText,
      createdAt: date,
      editedAt: ""
    }
    dispatch(addMessage(message));
    
    setMessageText('');
  }
  
  return (
    <div className="message-input">
      <input
        className="message-input-text"
        type="text" value={messageText}
        onChange={handleOnChange}
      />
      <button
        className="message-input-button"
        onClick={handleMessageAdd}
      >
        Send
      </button>
    </div>
  );
}

export default Input;