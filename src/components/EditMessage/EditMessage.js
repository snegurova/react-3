import {Link, useParams} from "react-router-dom";
import React, {useState} from "react";
import "./EditMessage.css";
import {useDispatch, useSelector} from "react-redux";
import {updateMessage} from "../../actions/actions";
import {searchByKey} from "../../helpers/helpers";

const EditMessage = () => {
  const {id} = useParams();
  const initialValue = useSelector(
    (state) => {
      return searchByKey('id', id, state.chat.chat.messages).text;
    }
  );
  const [messageText, setMessageText] = useState(initialValue);
  const dispatch = useDispatch();
  
  const handleOnChange = ({target}) => {
    setMessageText(target.value);
  }
  
  const handleMessageEdit = () => {
    dispatch(updateMessage(id, {text: messageText}));
  }
  return (
    <div className="message-input">
      <input className="message-input-text" type="text" value={messageText} onChange={handleOnChange}/>
      <Link to='/' className="message-input-button" onClick={handleMessageEdit}>Update</Link>
    </div>
  )
}

export default EditMessage;