import React, {useEffect} from "react";
import Header from "../Header/Header";
import './Chat.css'
import MessageList from "../MessageList/MessageList";
import Input from "../Input/Input";
import Preloader from "../Preloader/Preloader";
import {useDispatch, useSelector} from "react-redux";
import {fetchData, setChatData, setPreloader} from "../../actions/actions";
import {Route, Switch} from "react-router-dom";
import EditMessage from "../EditMessage/EditMessage";

const Chat = (props) => {
  const preloader = useSelector(
    (state) => state.chat.chat.preloader);
  
  const dispatch = useDispatch();
  useEffect(() => {
    fetch(props.url)
      .then(response => response.json())
      .then(messages => {
        const messagesSorted = messages.sort((a, b) => Date.parse(a['createdAt']) - Date.parse(b['createdAt']));
        return messagesSorted;
      })
      .then((messagesSorted) => {
        dispatch(fetchData(messagesSorted));
        dispatch(setChatData(messagesSorted));
        dispatch(setPreloader(false));
      })
      .catch((error) => console.log(error));
  }, [dispatch, props.url]);
  if (preloader) {
      return <Preloader/>;
  }
  return (
    <div className="chat-wrapper">
      <Header/>
      <Switch>
        <Route path='/edit/:id'>
          <EditMessage/>
        </Route>
        <Route path='/'>
          <MessageList/>
          <Input/>
        </Route>
      </Switch>
    </div>
  );
};

export default Chat;