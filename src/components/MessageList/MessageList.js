import React, {useEffect} from "react";
import './MessageList.css'
import Message from "../messages/Message";
import OwnMessage from "../messages/OwnMessage";
import {useDispatch, useSelector} from "react-redux";
import {setChatData} from "../../actions/actions";

const MessageList = () => {
  const messages = useSelector(
    (state) => state.chat.chat.messages
  );
  const currentUserId = useSelector(
    (state) => state.chat.chat.currentUserId
  );
  
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(setChatData(messages));
  });
  
  const massagesListItems = messages.map(it => {
    const date = new Date(it.createdAt);
    if (it.userId === currentUserId) {
      return <React.Fragment key={it.id}>
        <OwnMessage
          messageId={it.id}
          userName={it.user}
          messageTime={`${date.getHours()}:${date.getMinutes()}`}
          messageText={it.text}
        />
      </React.Fragment>;
    } else {
      return <React.Fragment key={it.id}>
        <Message
          avatarUrl={it.avatar}
          userName={it.user}
          messageTime={`${date.getHours()}:${date.getMinutes()}`}
          messageText={it.text}
        />
      </React.Fragment>;
    }
  });
  return (
    <React.Fragment>
      <ul className="message-list">
        {massagesListItems}
      </ul>
    </React.Fragment>
  );
};

export default MessageList;