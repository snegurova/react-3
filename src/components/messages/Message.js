import React, {useState} from "react";
import './Message.css'

const Message = ({ avatarUrl, userName, messageTime, messageText }) => {
  const [isLiked, setIsLiked] = useState(false);
  const handleClick = () => {
    setIsLiked(!isLiked);
  };
  const likeButtonText = isLiked ? 'liked' : 'like';
  const likeButtonClass = isLiked ? 'message-liked' : 'message-like';

  return (
      <li className="message">
        <div
          className="message-user-avatar"
          style={{backgroundImage: `url("${avatarUrl}")`}}
        >
        </div>
        <div className="message-right">
          <div className="message-info">
            <span className="message-user-name">{userName} </span>
            <span>at </span>
            <span className="message-time">{messageTime}</span>
          </div>
          <div className="message-text">
            {messageText}
          </div>
        </div>
        <button className={likeButtonClass} onClick={ handleClick }>{likeButtonText}</button>
      </li>
    );
};

export default Message;