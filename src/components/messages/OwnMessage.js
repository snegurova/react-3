import React from "react";
import './Message.css'
import {deleteMessage} from "../../actions/actions";
import {useDispatch} from "react-redux";
import {Link} from "react-router-dom";

const OwnMessage = ({messageId, userName, messageTime, messageText}) => {
  const dispatch = useDispatch();
  const handleDelete = () => {
    dispatch(deleteMessage(messageId));
  };
  return (
      <li className="own-message">
        <div className="message-info">
          <span className="message-user-name">{userName} </span>
          <span>at </span>
          <span className="message-time">{messageTime}</span>
        </div>
        <div className="own-message-text">
          {messageText}
        </div>
        <div className="own-message-buttons">
          <Link className="message-edit" to={`edit/${messageId}`}>Edit</Link>
          <button className="message-delete" onClick={ handleDelete }>Delete</button>
        </div>
      </li>
    );
};

export default OwnMessage;