import React from "react";
import './Header.css'
import {useSelector} from "react-redux";

const Header = () => {
  const chatTitle = useSelector(
    (state) => state.chat.chat.chatTitle
  );
  const usersCount = useSelector(
    (state) => state.chat.chat.usersCount
  );
  const messagesCount = useSelector(
    (state) => state.chat.chat.messagesCount
  );
  const lastMessageDate = useSelector(
    (state) => state.chat.chat.lastMessageDate
  );
    return (
      <header className="header">
        <div className="header-title">{chatTitle}</div>
        <div>
          <span className="header-users-count">{usersCount}</span>
          <span> participants</span>
        </div>
        <div>
          <span className="header-messages-count">{messagesCount}</span>
          <span> messages</span>
        </div>
        <div>
          <span>Last message at </span>
          <span className="header-last-message-date">{lastMessageDate}</span>
        </div>
        
      </header>
    );
}

export default Header;